import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Solution {
    public static int maximizeScore(ArrayList<Integer> arr, int n, int k) {
        int score = 0;
        
        if (n <= k) {
            for (int num : arr) {
                score += num;
            }
            return score;
        }
        
        // Pick K maximum elements from the start and end alternatively
        for (int i = 0; i < k; i++) {
            if (arr.get(n - 1) > arr.get(0)) {
                score += arr.get(n - 1);
                arr.remove(n - 1);
            } else {
                score += arr.get(0);
                arr.remove(0);
            }
            n--;
        }
        
        return score;
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt(); // Number of test cases
        
        while (t-- > 0) {
            int n = scanner.nextInt(); // Size of the array
            int k = scanner.nextInt(); // Maximum number of operations
            ArrayList<Integer> arr = new ArrayList<>();
            
            // Input array elements
            for (int i = 0; i < n; i++) {
                arr.add(scanner.nextInt());
            }
            
            // Get the maximum score
            int result = maximizeScore(arr, n, k);
            
            // Output the result
            System.out.println(result);
        }
        
        scanner.close();
    }
}
